import { Injectable } from '@angular/core';
import { Recipe } from './recipe.model';

@Injectable({
  providedIn: 'root'
})
export class RecipesService {
  private recipes: Recipe[] = [
    {
      id: 'r1',
      title: 'Ghirbas aux noix de coco',
      imageUrl: 'assets/img/r3.jpeg',
      ingredients: ['500g de noix de coco râpée', '200g de semoule', '350g de sucre glace', 'le zeste d\'un citron', '5 oeufs', '1/2 verre à thé d\'huile', '1/2 verre de beurre fondu', '1 sachet de levure chimique'],
      cuisson: '20 minutes',
      preparation: ['Dans un récipient, mélangez la noix de coco, la semoule, le sucre glace, le zeste de citron, les oeufs puis l\'huile et le beurre. Laissez reposer le mélange pendant une heure.',
                    'Ajouter la levure et mélanger bien. Faites des petites boulettes avec la pâte obtenue, aplatissez-les légèrement avec les doigts pour avoir la forme de ghriba. Saupoudrez de sucre glace puis disposez-les sur une plaque huilée et faites-les cuire dans un four préchauffé à 180°C.']
    },
    {
      id: 'r2',
      title: 'Ghirbas aux noix',
      imageUrl: 'assets/img/r2.jpeg',
      ingredients: ['500g de noix de coco râpée', '200g de semoule', '350g de sucre glace', 'le zeste d\'un citron', '5 oeufs', '1/2 verre à thé d\'huile', '1/2 verre de beurre fondu', '1 sachet de levure chimique'],
      cuisson: '20 minutes',
      preparation: ['Dans un récipient, mélangez la noix de coco, la semoule, le sucre glace, le zeste de citron, les oeufs puis l\'huile et le beurre. Laissez reposer le mélange pendant une heure.']
    }
  ];

  constructor() { }

  getAllRecipes() {
    return [...this.recipes];
  }

  getRecipe(recipeId: string) {
    return {
      ...this.recipes.find(recipe => {
        return recipe.id === recipeId;
      })
    };
  }
}
