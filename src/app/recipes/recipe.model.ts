export interface Recipe {
    id: string;
    title: string;
    imageUrl: string;
    ingredients: string[];
    cuisson: string;
    preparation: string[];
}
