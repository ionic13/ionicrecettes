import { Injectable } from '@angular/core';
import { Book } from './book.model';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  private books: Book[] = [
    {
      id: 'b1',
      title: 'Pâtisserie marocaine',
      imageUrl: 'assets/img/b1.jpeg',
      recettes: [
        {
          id: 'r1',
          title: 'Ghirbas aux noix de coco',
          imageUrl: 'assets/img/r3.jpeg',
          ingredients: ['500g de noix de coco râpée', '200g de semoule', '350g de sucre glace', 'le zeste d\'un citron', '5 oeufs', '1/2 verre à thé d\'huile', '1/2 verre de beurre fondu', '1 sachet de levure chimique'],
          cuisson: '20 minutes',
          preparation: ['Dans un récipient, mélangez la noix de coco, la semoule, le sucre glace, le zeste de citron, les oeufs puis l\'huile et le beurre. Laissez reposer le mélange pendant une heure.',
                        'Ajouter la levure et mélanger bien. Faites des petites boulettes avec la pâte obtenue, aplatissez-les légèrement avec les doigts pour avoir la forme de ghriba. Saupoudrez de sucre glace puis disposez-les sur une plaque huilée et faites-les cuire dans un four préchauffé à 180°C.']
        },
        {
          id: 'r2',
          title: 'Ghirbas aux noix',
          imageUrl: 'assets/img/r2.jpeg',
          ingredients: ['500g de noix de coco râpée', '200g de semoule', '350g de sucre glace', 'le zeste d\'un citron', '5 oeufs', '1/2 verre à thé d\'huile', '1/2 verre de beurre fondu', '1 sachet de levure chimique'],
          cuisson: '20 minutes',
          preparation: ['Dans un récipient, mélangez la noix de coco, la semoule, le sucre glace, le zeste de citron, les oeufs puis l\'huile et le beurre. Laissez reposer le mélange pendant une heure.']
        }
      ]
    },
    {
      id: 'b2',
      title: 'L\'heure du thé',
      imageUrl: 'assets/img/b2.jpeg',
      recettes: [
      ]
    },
    {
      id: 'b3',
      title: 'Gâteaux familiaux au chocolat',
      imageUrl: 'assets/img/b3.jpeg',
      recettes: [
      ]
    },

  ];

  constructor() { }

  getAllBooks() {
    return [...this.books];
  }

  getBook(bookId: string) {
    return {
      ...this.books.find(book => {
        return book.id === bookId;
      })
    };
  }
}
