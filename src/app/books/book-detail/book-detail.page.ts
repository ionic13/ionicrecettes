import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BooksService } from '../books.service';
import { Book } from '../book.model';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.page.html',
  styleUrls: ['./book-detail.page.scss'],
})
export class BookDetailPage implements OnInit {

  loadedBook: Book;
  loading = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private booksService: BooksService,
    private router: Router) {
  }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(paramMap => {
      if (!paramMap.has('bookId')) {
        this.router.navigate(['/books']);
        return;
      }

      const bookId = paramMap.get('bookId');
      this.loadedBook = this.booksService.getBook(bookId);
      this.loading = false;
    });
  }
}
