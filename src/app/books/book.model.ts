import { Recipe } from './../recipes/recipe.model';
export interface Book {
    id: string;
    title: string;
    imageUrl: string;
    recettes: Recipe[];
}
